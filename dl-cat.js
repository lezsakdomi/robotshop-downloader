#!/usr/bin/env node
const rp = require('request-promise');
const {AllHtmlEntities: entities} = require('html-entities');
const {MongoClient} = require('mongodb');

class RobotshopSpider {
    constructor(databaseName = "robotshop", mongoUri = "mongodb://localhost:27017") {
        this.mongo = MongoClient.connect(mongoUri);
        this.db = this.mongo.then(mongo => mongo.db(databaseName));
        this.offline = false;
        this.reparse = false;
    }

    static async fetchAndParse(url, f) {
        process.stdout.write(url);
        const s = await rp(url);
        process.stdout.write(" OK ");
        return f.call(this, url, s);
    }

    static justParsePage(url, s) {
        const products = [];
        const prodUrlRegex = /<a href="([^"]*)" title="([^"]*)">\r\n/g;
        {
            let match;
            while (match = prodUrlRegex.exec(s)) {
                products.push({
                    title: match[2],
                    url: match[1],
                });
                process.stdout.write(".");
            }
        }
        process.stdout.write("\n");
        return {
            url: url,
            original: s,
            title: entities.decode(s.match(/<h1>(.*)<\/h1>/)[1]),
            nextPageUrl: (() => {
                const m = s.match(/<a class="next i-next" href="([^"]*)" title="Next">/);
                return m === null ? null : entities.decode(m[1]);
            })(),
            pageNum: s.match(/<li class="current">([0-9]+)<\/li>/)[1],
            products: products,
        }
    }

    static justParseProduct(url, s) {
        const result = {
            url: url,
            original: s,
            title: entities.decode(s.match(/<h1>(.*)<\/h1>/)[1]),
            data: {},
        };

        let m;
        if (m = s.match(/(?:Size: )?([0-9.]+)D x ([0-9.]+)L mm/i)) {
            result.data.size = {D: parseFloat(m[1]), L: parseFloat(m[2])};
            process.stdout.write('S');
        } else {
            process.stdout.write('s');
        }
        if (m = s.match(/Weight: ([0-9.]+) ?g/i)) {
            result.data.weight = parseInt(m[1]);
            process.stdout.write('W');
        } else {
            process.stdout.write('w');
        }
        if (m = s.match(/Shaft Diameter: ([0-9.]+) ?mm/i)) {
            result.data.shaftD = parseFloat(m[1]);
            process.stdout.write('S');
        } else {
            process.stdout.write('s');
        }
        if (m = s.match(/Shaft Length: ([0-9.]+) ?mm/i)) {
            result.data.shaftL = parseFloat(m[1]);
            process.stdout.write('S');
        } else {
            process.stdout.write('s');
        }
        if (m = s.match(/Nominal Voltage: ([0-9]+) ?V/i)) {
            result.data.voltage = parseInt(m[1]);
            process.stdout.write('V');
        } else {
            process.stdout.write('v');
        }
        if (m = s.match(/No Load RPM: ([0-9]+)(?: ?rpm)?/i)) {
            result.data.nRPM = parseInt(m[1]);
            process.stdout.write('R');
        } else {
            process.stdout.write('r');
        }
        if (m = s.match(/No Load Current: ([0-9.]+) ?A/i)) {
            result.data.nCurrent = parseFloat(m[1]);
            process.stdout.write('A');
        } else {
            process.stdout.write('a');
        }
        if (m = s.match(/No Load RPM: ([0-9]+)(?: ?rpm)?/i)) {
            result.data.nRPM = parseInt(m[1]);
            process.stdout.write('R');
        } else {
            process.stdout.write('r');
        }
        if (m = s.match(/Stall RPM: ([0-9]+)(?: ?rpm)?/i)) {
            result.data.sRPM = parseInt(m[1]);
            process.stdout.write('R');
        } else {
            process.stdout.write('r');
        }
        if (m = s.match(/Stall Current: ([0-9.]+) ?A/i)) {
            result.data.sCurrent = parseFloat(m[1]);
            process.stdout.write('A');
        } else {
            process.stdout.write('a');
        }
        if (m = s.match(/Rated RPM: ([0-9]+)(?: ?rpm)?/i)) {
            result.data.RPM = parseInt(m[1]);
            process.stdout.write('R');
        } else {
            process.stdout.write('r');
        }
        if (m = s.match(/Rated Current: ([0-9.]+) ?A/i)) {
            result.data.current = parseFloat(m[1]);
            process.stdout.write('A');
        } else {
            process.stdout.write('a');
        }
        //if(m=s.match(/<li><\/li>/)){result.data.;process.stdout.write('');}
        // else process.stdout.write('');

        process.stdout.write("\n");
        return result;
    }

    async parseCategory(url) {
        const page = await this.parsePage(url);
        const pageProducts = [];
        for (let i = 0; i < page.products.length; i++) {
            const product = await this.parseProduct(page.products[i].url);
            if (!product) continue;
            pageProducts.push(product);
        }
        const additionalProducts = [];
        if (page.nextPageUrl) {
            additionalProducts.push(...(await this.parseCategory(page.nextPageUrl)).products);
        }
        return {
            url: url,
            title: page.title,
            products: [...pageProducts, ...additionalProducts],
        }
    }

    async cachedParse(collectionName, url, f) {
        const collection = (await this.db).collection(collectionName);
        let result = await collection.findOne({_id: url});
        if (result == null) {
            if (this.offline) {
                console.log(`Trying to get ${url}, but it isn't cached`);
                return null;
            }
            result = await RobotshopSpider.fetchAndParse.call(this, f);
            collection.insertOne({
                ...result,
                _id: url,
            })
                .catch(e => console.warn(
                    `mongodb: already inserted ${url} (possible race condition): ${e}`));
        } else {
            if (this.reparse) {
                result = f.call(this, url, result.original);
                collection.replaceOne({_id: url}, {
                    ...result,
                    _id: url,
                })
                    .catch(e => console.error(`Failed to update ${url}`, e));
            }
        }
        return result
    }

    async parsePage(url) {
        return this.cachedParse('pages', url, RobotshopSpider.justParsePage)
    }

    async parseProduct(url) {
        return this.cachedParse('products', url, RobotshopSpider.justParseProduct)
    }

    async destroy() {
        return (await this.mongo).close(false)
    }
}

module.exports = RobotshopSpider;

// noinspection JSUnusedLocalSymbols
function compare(a, b) {
    // noinspection EqualityComparisonWithCoercionJS
    return a == b ? 0 : a < b ? -1 : 1;
}

async function main() {
    const [, self, url, output_file] = process.argv;
    if (!url) {
        console.error(`Usage: ${self} url [output_file]`);
        process.exit(1);
    }
    const robotshopSpider = new RobotshopSpider();
    try {
        const url = process.argv[2];
        const parsed = await robotshopSpider.parseCategory(url);
        if (output_file) {
            const fs = require('fs');
            console.log("Writing to file...");
            fs.writeFileSync(output_file, JSON.stringify(parsed, null, 4));
        } else {
            console.log(parsed.products.map(product => ({
                url: product.url,
                title: product.title,
                data: product.data,
            })));
        }
    } finally {
        await robotshopSpider.destroy();
    }
}

if (require.main === module) {
    // noinspection JSIgnoredPromiseFromCall
    main();
}
