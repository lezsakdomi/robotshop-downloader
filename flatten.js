#!/usr/bin/env node
const [, self, input_file, output_file] = process.argv;
if (!input_file || !output_file) throw new Error(`Usage: ${self} input_path output_path`);

const fs = require('fs');
console.log("Reading file...");
let obj = JSON.parse(fs.readFileSync(input_file));
console.log("Flattening...");
obj = obj.products.map(p => {
    delete p._id;
    delete p.original;
    const d = p.data;
    delete p.data;
    if (d.size) {
        d.sizeD = d.size.D;
        d.sizeL = d.size.L;
        delete d.size;
    }
    return {
        ...p,
        ...d,
    };
});
console.log("Writing...");
fs.writeFileSync(output_file, JSON.stringify(obj, null, 4));
console.log("Done");